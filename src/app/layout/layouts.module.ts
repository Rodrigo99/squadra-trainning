import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {FormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
import {MatSelectModule} from '@angular/material/select';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';


import {LayoutComponent} from './layout.component';
import {HeaderComponent} from './header/header.component';
import {FooterComponent} from './footer/footer.component';
import {LoaderDialogComponent} from "./loader-dialog/loader-dialog.component";
import {MessageModule} from "../shared/message/message.module";
import {FlexLayoutModule} from "@angular/flex-layout";
import {MatMenuModule} from "@angular/material/menu";


/**
 * @author GF
 */
@NgModule({
    declarations: [
        LayoutComponent,
        HeaderComponent,
        FooterComponent,
        LoaderDialogComponent],
    imports: [
        FormsModule,
        CommonModule,
        FlexLayoutModule,
        RouterModule,
        MessageModule,
        MatButtonModule,
        MatDialogModule,
        MatSelectModule,
        FlexLayoutModule,
        MatProgressSpinnerModule,
        MatMenuModule,
    ],
    entryComponents: [
        LoaderDialogComponent
    ]
})
export class LayoutModule {
}
