import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

/**
 * @author GF
 */
@Component({
  selector: 'app-loader-dialog',
  templateUrl: './loader-dialog.component.html'
})
export class LoaderDialogComponent {

  /**
   * Construtor da classe.
   *
   * @param dialogRef
   */
  constructor(public dialogRef: MatDialogRef<LoaderDialogComponent>) { }

}
