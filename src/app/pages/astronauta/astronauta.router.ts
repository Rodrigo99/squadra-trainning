import {Routes} from '@angular/router';
import {AstronautaFormComponent} from "./astronauta-form/astronauta-form.component";
import {AstronautaListComponent} from "./astronauta-list/astronauta-list.component";
import {AstronautaResolve} from "./shared/astronauta-client/astronauta.resolve";


/**
 * Configurações de rota de Astronauta.
 *
 * @author GF
 */
export const AstronautaRoutes: Routes = [
    {
        path: 'incluir',
        component: AstronautaFormComponent,
        data: {
            'acao': 'incluir',
        }
    },
    {
        path: ':id/alterar',
        component: AstronautaFormComponent,
        data: {
            'acao': 'alterar',
        },
        resolve: {
            astronauta: AstronautaResolve
        }
    },
    {
        path: 'listar',
        component: AstronautaListComponent,
    },
    {
        path: '',
        redirectTo: 'listar',
        pathMatch: 'full'
    }
];
