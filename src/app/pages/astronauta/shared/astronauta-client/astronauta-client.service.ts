import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from "../../../../../environments/environment";

/**
 * Classe de integração com o serviço .
 */
@Injectable({
    providedIn: 'root'
})
export class AstronautaClientService {

    /**
     * Construtor da classe.
     *
     * @param http
     */
    constructor(private http: HttpClient) {
    }

    /**
     * Salva a instância de Astronauta.
     *
     * @param astronauta
     * @return
     */
    public salvar(astronauta: any): Observable<any> {
        let result: Observable<any> = null;

        if (astronauta.id) {
            result = this.http.put(`${environment.urlApi}Astronauta`, astronauta);
        } else {
            result = this.http.post(`${environment.urlApi}Astronauta`, astronauta);
        }
        return result;
    }

    /**
     * Inativa a Astronauta confomre o 'id' informado.
     *
     * @param astronauta
     * @return
     */
    public excluir(astronauta): Observable<any> {
        return this.http.delete(`${environment.urlApi}Astronauta`, {
            params: {id: astronauta.id}
        });
    }

    /**
     * Retorna uma instância de Astronauta conforme o 'id' informado.
     * @param id
     * @return
     */
    public obterPorId(id: number): Observable<any> {
        return this.http.get(`${environment.urlApi}Astronauta/${id}`);
    }

    /**
     * Retorna o array de astronauta confome o filtro de pesquisa informado.
     *
     */
    public filtrar(astronauta): Observable<any> {
        return this.http.get(`${environment.urlApi}Astronauta`, {
            params: astronauta
        });
    }

}
