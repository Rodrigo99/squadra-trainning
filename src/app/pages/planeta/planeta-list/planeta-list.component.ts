import {Component, OnInit} from '@angular/core';
import {MessageService} from "../../../shared/message/message.service";
import {NgForm} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {PlanetaClientService} from "../shared/planeta-client/planeta-client.service";

@Component({
    selector: 'app-planeta-list',
    templateUrl: './planeta-list.component.html'
})
export class PlanetaListComponent implements OnInit {

    public planeta: any = {};

    public planetas: any = [];

    /**
     * Construtor da classe
     *
     * @param route
     * @param router
     * @param messageService
     * @param planetaClientService
     */
    constructor(
        route: ActivatedRoute,
        private router: Router,
        private messageService: MessageService,
        private planetaClientService: PlanetaClientService
    ) {
    }

    ngOnInit() {
        this.pesquisar(this.planeta)
    }

    /**
     * Responsável por pesquisar os itens.
     *
     * @param planeta
     */
    public pesquisar(planeta: any) {
        this.planetaClientService.filtrar(planeta).subscribe((data) => {
            this.planetas = data.result
        }, error => {
            this.messageService.addMsgDanger(error.errorMessage);
        });

    }

    /**
     * Responsável por excluir um item.
     *
     * @param planeta
     */
    public excluir(planeta: any): void {
        this.messageService.addConfirmYesNo('MSG036', () => {
            this.planetaClientService.excluir(planeta).subscribe(() => {
                this.pesquisar(this.planeta);
                this.messageService.addMsgSuccess("MSG003");
            }, error => {
                this.messageService.addMsgDanger(error);
            });
        });
    }


}
