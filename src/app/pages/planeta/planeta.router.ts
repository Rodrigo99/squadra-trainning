import {Routes} from '@angular/router';
import {PlanetaFormComponent} from "./planeta-form/planeta-form.component";
import {PlanetaListComponent} from "./planeta-list/planeta-list.component";
import {PlanetaResolve} from "./shared/planeta-client/planeta.resolve";

/**
 * Configurações de rota de Planeta.
 *
 * @author GF
 */
export const PlanetaRoutes: Routes = [
    {
        path: 'incluir',
        component: PlanetaFormComponent,
        data: {
            'acao': 'incluir',
        }
    },
    {
        path: ':id/alterar',
        component: PlanetaFormComponent,
        data: {
            'acao': 'alterar',
        },
        resolve: {
            planeta: PlanetaResolve
        }
    },
    {
        path: 'listar',
        component: PlanetaListComponent,
    },
    {
        path: '',
        redirectTo: 'listar',
        pathMatch: 'full'
    }
];
