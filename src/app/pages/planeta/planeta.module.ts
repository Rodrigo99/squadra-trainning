import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PlanetaFormComponent} from "./planeta-form/planeta-form.component";
import {PlanetaListComponent} from "./planeta-list/planeta-list.component";
import {PlanetaRoutes} from "./planeta.router";
import {RouterModule} from "@angular/router";
import {MatCardModule} from "@angular/material/card";
import {FlexModule} from "@angular/flex-layout";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {FormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {MessageModule} from "../../shared/message/message.module";
import {PlanetaClientModule} from "./shared/planeta-client/planeta-client.module";
import {CdkTableModule} from "@angular/cdk/table";
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";

/**
 * @author GF
 */
@NgModule({
    declarations: [PlanetaFormComponent, PlanetaListComponent],
    imports: [
        CommonModule,
        RouterModule.forChild(PlanetaRoutes),
        MatCardModule,
        FlexModule,
        MatFormFieldModule,
        PlanetaClientModule,
        MatSelectModule,
        FormsModule,
        MatButtonModule,
        MatInputModule,
        MessageModule,
        CdkTableModule,
        MatTableModule,
        MatIconModule
    ]
})
export class PlanetaModule {
}
