import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PlanetaClientService} from './planeta-client.service';
import {PlanetaResolve} from "./planeta.resolve";


/**
 * Modulo de integração do projeto frontend com os serviços backend.
 */
@NgModule({
    declarations: [],
    imports: [
        CommonModule,
    ],
    providers: [
        PlanetaClientService,
        PlanetaResolve
    ]
})
export class PlanetaClientModule {
}
