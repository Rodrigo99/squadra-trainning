import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from "../../../../../environments/environment";

/**
 * Classe de integração com o serviço .
 */
@Injectable({
    providedIn: 'root'
})
export class PlanetaClientService {

    /**
     * Construtor da classe.
     *
     * @param http
     */
    constructor(private http: HttpClient) {
    }

    /**
     * Salva a instância de Planeta.
     *
     * @param planeta
     * @return
     */
    public salvar(planeta: any): Observable<any> {
        let result: Observable<any> = null;

        if (planeta.id) {
            result = this.http.put(`${environment.urlApi}Planeta`, planeta);
        } else {
            result = this.http.post(`${environment.urlApi}Planeta`, planeta);
        }
        return result;
    }

    /**
     * Inativa a Planeta confomre o 'id' informado.
     *
     * @param planeta
     * @return
     */
    public excluir(planeta): Observable<any> {
        return this.http.delete(`${environment.urlApi}Planeta`, {
            params: {id: planeta.id}
        });
    }

    /**
     * Retorna uma instância de Planeta conforme o 'id' informado.
     * @param id
     * @return
     */
    public obterPorId(id: number): Observable<any> {
        return this.http.get(`${environment.urlApi}Planeta/${id}`);
    }

    /**
     * Retorna o array de planetas confome o filtro de pesquisa informado.
     *
     */
    public filtrar(planeta): Observable<any> {
        return this.http.get(`${environment.urlApi}Planeta`, {
            params: planeta
        });
    }

}
