import {BrowserModule} from '@angular/platform-browser';
import {NgModule, LOCALE_ID} from '@angular/core';
import {AppMessage} from './app.message';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LayoutModule} from './layout/layouts.module';
import {MessageResourceProvider} from "./shared/message/message.resource";
import {MessageModule} from "./shared/message/message.module";

import {MatDialogModule} from '@angular/material/dialog';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import localeBr from '@angular/common/locales/pt';
import {LoaderModule} from './shared/loader/loader.module';
import {ConfirmModule} from './layout/confirm/confirm.module';
import {ValidationModule} from './shared/validation/validation.module';
import {ValidationResourceProvider} from './shared/validation/validation.resource';

import {HashLocationStrategy, LocationStrategy, registerLocaleData} from '@angular/common';
import {AppInterceptor} from "./app.interceptor";
import {ManagementEventService} from "./shared/services/management-event.service";

registerLocaleData(localeBr, 'pt-br');


/**
 * Init Locale Date.
 */

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    LoaderModule,
    ConfirmModule,
    LayoutModule,
    BrowserModule,
    ValidationModule,
    MatDialogModule,
    HttpClientModule,
    AppRoutingModule,
    MessageModule.forRoot(),
    BrowserAnimationsModule,
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {
      provide: LOCALE_ID,
      useValue: 'pt-br'
    },
    {
      provide: MessageResourceProvider,
      useValue: AppMessage
    },
    {
      provide: ValidationResourceProvider,
      useValue: AppMessage,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AppInterceptor,
      multi: true
    },
    ManagementEventService
  ],
  exports: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule {

}
