import {InternacionalizacaoResource} from './shared/message/internacionalizacao.resource';
import {FormControl, NgForm} from '@angular/forms';

/**
 * Implementação responsável por prover as 'descrições' e 'mensagens' utilizadas na aplicação em um único local.
 *
 * @author GF
 */
export class AppMessage implements InternacionalizacaoResource {

  private resource: object;

  /**
   * Construtor da classe.
   */
  constructor() {
    this.resource = {
      // LABEL
      LABEL_OK: 'OK',
      LABEL_NAO_OBSERVADO: 'Não Observado',
      LABEL_SAIR: 'Sair',
      LABEL_SIM: 'Sim',
      LABEL_NAO: 'Não',
      LABEL_PESQUISAR: 'Pesquisar',
      LABEL_LIMPAR: 'Limpar',
      LABEL_SALVAR: 'Salvar',
      LABEL_CANCELAR: 'Cancelar',
      LABEL_CONSULTAR: 'Consultar',

      // MSG
      MSG003: 'Operação realizada com sucesso.',
      MSG036: 'Deseja excluir o registro?',

      // Validation
      required: 'Campo de preenchimento obrigatório.',
    };
  }

  /**
   * Retorna a 'descrição' conforme a 'chave' informada.
   *
   * @param key -
   * @returns -
   */
  getDescription(key: string): string {
    return this.resource[key];
  }

  /**
   * Retorna a 'mensagem' conforme a 'chave' informada.
   *
   * @param key -
   * @returns -
   */
  getMessage(key: string): string {
    return this.getDescription(key);
  }
}

