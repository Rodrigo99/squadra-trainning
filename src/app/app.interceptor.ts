import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';

import { Message } from './shared/message/message.service';

/**
 * Implementação responsável por interceptar as requisições Http.
 */
@Injectable()
export class AppInterceptor implements HttpInterceptor {

    /**
     * Construtor da classe.
     */
    constructor() { }

    /**
     * Método responsável por interceptar a requisição Http.
     * 
     * @param request 
     * @param next 
     */
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(catchError((response: HttpErrorResponse): Observable<HttpEvent<Message>> => {
            let message = Object.assign(new Message(), response.error);
            return throwError(message);
        }));
    }
}
